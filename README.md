# Exodus
自由的跨平台代理软件。

## 目标
### Core
- [ ] Shadowsocks
- [ ] Simple-obfs
- [ ] Surge config file compatibility
- [ ] GEOIP (CN)
- [ ] IPv6
- [ ] UDP
- [ ] TCP
- [ ] Redir
- [ ] V2ray

### Core (ARM)
- [ ] Shadowsocks
- [ ] Simple-obfs
- [ ] Surge config file compatibility
- [ ] GEOIP (CN)
- [ ] IPv6
- [ ] UDP
- [ ] TCP
- [ ] Redir
- [ ] V2ray

### Android
- [ ] Kotlin?

### Desktop
- [ ] Electron

## Roadmap
- [x] Choose a license
- [ ] Actually code something
