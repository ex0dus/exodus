use exodus::{TargetAddress, TargetHost};
use num_derive::FromPrimitive;
use num_traits::FromPrimitive;
use std::net::{Ipv4Addr, Ipv6Addr};
use tokio::io::AsyncReadExt;
use tokio::net::TcpStream;

pub const VER_SOCKS5: u8 = 5;
pub const NO_AUTH: u8 = 0;
pub const CMD_CONNECT: u8 = 1;

#[derive(thiserror::Error, Debug)]
pub enum Error {
    #[error("unsupport version")]
    UnsupportVersion,
    #[error("unsupport auth method")]
    InvalidTargetAddress,
    #[error("invalid target address")]
    UnsupportAuthMethod,
    #[error("connect target fail")]
    ConnectFail,
    #[error("unsupport request command")]
    UnsupportCommand,
    #[error("ParmaError: {0}")]
    ParamError(String),
    #[error("{0:?}")]
    IO(#[from] std::io::Error),
    #[error("{0:?}")]
    Parse(#[from] std::net::AddrParseError),
    #[error("{0:?}")]
    UTF8(#[from] std::str::Utf8Error),
}

// In an address field (DST.ADDR, BND.ADDR), the ATYP field specifies
// the type of address contained within the field:
//
//        o  X'01'
//
// the address is a version-4 IP address, with a length of 4 octets
//
//        o  X'03'
//
// the address field contains a fully-qualified domain name.  The first
// octet of the address field contains the number of octets of name that
// follow, there is no terminating NUL octet.
//
//        o  X'04'
//
// the address is a version-6 IP address, with a length of 16 octets.

#[derive(FromPrimitive)]
pub enum ATYP {
    IPV4 = 1,
    IPV6 = 4,
    DOMAIN = 3,
}

pub fn write_address(buf: &mut Vec<u8>, addr: &TargetAddress) -> Result<(), Error> {
    match &addr.host {
        TargetHost::V4(host) => {
            buf.push(ATYP::IPV4 as u8);
            buf.extend_from_slice(&host.octets());
        }
        TargetHost::V6(host) => {
            buf.push(ATYP::IPV6 as u8);
            buf.extend_from_slice(&host.octets());
        }
        TargetHost::Domain(host) => {
            if host.len() > 255 {
                return Err(Error::InvalidTargetAddress);
            }
            buf.push(ATYP::DOMAIN as u8);
            buf.push(host.len() as u8);
            buf.extend_from_slice(&host.as_bytes());
        }
    };
    buf.push((addr.port >> 8) as u8);
    buf.push(addr.port as u8);
    Ok(())
}

pub async fn read_address(conn: &mut TcpStream) -> Result<TargetAddress, Error> {
    let mut buff = [0u8];
    conn.read_exact(&mut buff).await?;
    let [atyp] = buff;
    match FromPrimitive::from_u8(atyp) {
        Some(ATYP::IPV4) => {
            let mut buff = [0u8; 6];
            conn.read_exact(&mut buff).await?;
            let addr = Ipv4Addr::new(buff[0], buff[1], buff[2], buff[3]);
            Ok(TargetAddress {
                host: TargetHost::V4(addr),
                port: ((buff[4] as u16) << 8) | (buff[5] as u16),
            })
        }
        Some(ATYP::IPV6) => {
            let mut buff = [0u8; 18];
            conn.read_exact(&mut buff).await?;
            let mut addr_buf = [0u8; 16];
            addr_buf.copy_from_slice(&buff[0..16]);
            let addr = Ipv6Addr::from(addr_buf);
            Ok(TargetAddress {
                host: TargetHost::V6(addr),
                port: ((buff[16] as u16) << 8) | (buff[17] as u16),
            })
        }
        Some(ATYP::DOMAIN) => {
            let mut buff = [0u8];
            conn.read_exact(&mut buff).await?;
            let domain_len = buff[0] as usize;
            let mut buff = vec![0u8; domain_len + 2];
            conn.read_exact(&mut buff).await?;
            let domain = std::str::from_utf8(&buff[0..domain_len])?;
            Ok(TargetAddress {
                host: TargetHost::Domain(String::from(domain)),
                port: ((buff[domain_len] as u16) << 8) | (buff[domain_len + 1] as u16),
            })
        }
        None => Err(Error::ParamError(String::from("Invalid Address Type"))),
    }
}
