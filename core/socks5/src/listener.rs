use crate::protocol::*;
use async_trait::async_trait;
use exodus;
use exodus::{Dispatcher, TargetAddress, TargetHost};
use std::sync::Arc;
use tokio::io::{AsyncReadExt, AsyncWriteExt};
use tokio::net::{TcpListener, TcpStream, ToSocketAddrs};

pub struct Listener {
    tcp: TcpListener,
}

impl Listener {
    pub async fn new<A: ToSocketAddrs>(addr: A) -> Result<Self, Error> {
        let tcp = TcpListener::bind(addr).await?;
        Ok(Listener { tcp })
    }
    fn spawn(mut conn: TcpStream, dispatcher: Arc<Dispatcher>) {
        tokio::spawn(async move {
            match handle(&mut conn).await {
                Ok(addr) => {
                    futures::pin_mut!(conn);
                    dispatcher.redirect(&addr, conn).await;
                }
                Err(e) => eprintln!("handle error: {:?}", e),
            };
        });
    }
}

#[async_trait]
impl exodus::Listener for Listener {
    async fn accept(&mut self, dispatcher: Arc<Dispatcher>) {
        match self.tcp.accept().await {
            Ok((conn, _)) => Self::spawn(conn, dispatcher),
            Err(e) => eprintln!("accept error: {:?}", e),
        }
    }
}

async fn handle(conn: &mut TcpStream) -> Result<TargetAddress, Error> {
    negotiate(conn).await?;
    read_request(conn).await?;
    let addr = read_address(conn).await?;
    // TODO: We should reply error instead of return when any error happend
    // during previous processing (until async closures become stable?)
    write_reply(conn, true).await?;
    Ok(addr)
}

/// The client connects to the server, and sends a version
/// identifier/method selection message:
///
///                 +----+----------+----------+
///                 |VER | NMETHODS | METHODS  |
///                 +----+----------+----------+
///                 | 1  |    1     | 1 to 255 |
///                 +----+----------+----------+
///
/// The VER field is set to X'05' for this version of the protocol.  The
/// NMETHODS field contains the number of method identifier octets that
/// appear in the METHODS field.
///
/// The server selects from one of the methods given in METHODS, and
/// sends a METHOD selection message:
///
///                       +----+--------+
///                       |VER | METHOD |
///                       +----+--------+
///                       | 1  |   1    |
///                       +----+--------+
///
/// If the selected METHOD is X'FF', none of the methods listed by the
/// client are acceptable, and the client MUST close the connection.
///
/// The values currently defined for METHOD are:
///
///        o  X'00' NO AUTHENTICATION REQUIRED
///        o  X'01' GSSAPI
///        o  X'02' USERNAME/PASSWORD
///        o  X'03' to X'7F' IANA ASSIGNED
///        o  X'80' to X'FE' RESERVED FOR PRIVATE METHODS
///        o  X'FF' NO ACCEPTABLE METHODS
///
/// The client and server then enter a method-specific sub-negotiation.
///
/// Descriptions of the method-dependent sub-negotiations appear in
/// separate memos.
///
/// Developers of new METHOD support for this protocol should contact
/// IANA for a METHOD number.  The ASSIGNED NUMBERS document should be
/// referred to for a current list of METHOD numbers and their
/// corresponding protocols.
///
/// Compliant implementations MUST support GSSAPI and SHOULD support
/// USERNAME/PASSWORD authentication methods.
async fn negotiate(conn: &mut TcpStream) -> Result<(), Error> {
    // TODO: Support AUTH
    let mut var_nmethods = [0u8; 2];
    conn.read_exact(&mut var_nmethods).await?;
    let [ver, nmethods] = var_nmethods;
    if ver != VER_SOCKS5 {
        return Err(Error::UnsupportVersion);
    }
    let mut methods = vec![0u8; nmethods as usize];
    conn.read_exact(&mut methods).await?;
    if !methods.contains(&NO_AUTH) {
        return Err(Error::UnsupportAuthMethod);
    }
    conn.write_all(&[VER_SOCKS5, NO_AUTH]).await?;
    Ok(())
}

/// Once the method-dependent subnegotiation has completed, the client
/// sends the request details.  If the negotiated method includes
/// encapsulation for purposes of integrity checking and/or
/// confidentiality, these requests MUST be encapsulated in the method-
/// dependent encapsulation.
///
/// The SOCKS request is formed as follows:
///
///      +----+-----+-------+------+----------+----------+
///      |VER | CMD |  RSV  | ATYP | DST.ADDR | DST.PORT |
///      +----+-----+-------+------+----------+----------+
///      | 1  |  1  | X'00' |  1   | Variable |    2     |
///      +----+-----+-------+------+----------+----------+
///
///   Where:
///
///        o  VER      protocol version: X'05'
///        o  CMD
///           o  CONNECT X'01'
///           o  BIND X'02'
///           o  UDP ASSOCIATE X'03'
///        o  RSV      RESERVED
///        o  ATYP     address type of following address
///           o  IP V4 address: X'01'
///           o  DOMAINNAME: X'03'
///           o  IP V6 address: X'04'
///        o  DST.ADDR desired destination address
///        o  DST.PORT desired destination port in network octet
///           order
///
/// The SOCKS server will typically evaluate the request based on source
/// and destination addresses, and return one or more reply messages, as
/// appropriate for the request type.
async fn read_request(conn: &mut TcpStream) -> Result<(), Error> {
    // TODO: Support Bind & UDP
    let mut buf = [0u8; 3];
    conn.read_exact(&mut buf).await?;
    let [ver, cmd, _rsv] = buf;
    if ver != VER_SOCKS5 {
        return Err(Error::UnsupportAuthMethod);
    }
    if cmd != CMD_CONNECT {
        return Err(Error::UnsupportCommand);
    }
    Ok(())
}

/// The SOCKS request information is sent by the client as soon as it has
/// established a connection to the SOCKS server, and completed the
/// authentication negotiations.  The server evaluates the request, and
/// returns a reply formed as follows:
///
///      +----+-----+-------+------+----------+----------+
///      |VER | REP |  RSV  | ATYP | BND.ADDR | BND.PORT |
///      +----+-----+-------+------+----------+----------+
///      | 1  |  1  | X'00' |  1   | Variable |    2     |
///      +----+-----+-------+------+----------+----------+
///
///   Where:
///
///        o  VER    protocol version: X'05'
///        o  REP    Reply field:
///           o  X'00' succeeded
///           o  X'01' general SOCKS server failure
///           o  X'02' connection not allowed by ruleset
///           o  X'03' Network unreachable
///           o  X'04' Host unreachable
///           o  X'05' Connection refused
///           o  X'06' TTL expired
///           o  X'07' Command not supported
///           o  X'08' Address type not supported
///           o  X'09' to X'FF' unassigned
///        o  RSV    RESERVED
///        o  ATYP   address type of following address
///           o  IP V4 address: X'01'
///           o  DOMAINNAME: X'03'
///           o  IP V6 address: X'04'
///        o  BND.ADDR       server bound address
///        o  BND.PORT       server bound port in network octet order
///
/// Fields marked RESERVED (RSV) must be set to X'00'.
async fn write_reply(conn: &mut TcpStream, success: bool) -> Result<(), Error> {
    let mut buf = Vec::new();
    buf.push(VER_SOCKS5);
    // TODO: Support more error types
    buf.push(match success {
        true => 0,
        false => 1, // general SOCKS server failure
    });
    buf.push(0); // RESERVED

    // We doesn't use BND.ADDR and BND.PORT. So just leave them all 0.
    // Use IPV4 because it's simplest.
    let unspecified = &TargetAddress {
        host: TargetHost::V4(std::net::Ipv4Addr::UNSPECIFIED),
        port: 0,
    };
    write_address(&mut buf, unspecified)?;
    conn.write_all(&buf).await?;
    Ok(())
}
