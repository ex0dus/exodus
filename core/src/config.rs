use serde::{Deserialize, Serialize};
use socks5;
use std::collections::HashMap;
use std::fs::File;
use std::string::String;

#[derive(Deserialize, Serialize, PartialEq, Debug)]
pub struct Config {
    pub mode: String,
    pub log: String,
    pub listen: Listen,
    pub proxies: HashMap<String, Redirector>,
    pub rules: Vec<String>,
}

#[derive(Serialize, Deserialize, PartialEq, Debug)]
#[serde(tag = "type")]
pub enum Listen {
    #[serde(rename = "socks5")]
    Socks5 { addr: String },
    #[serde(rename = "tun")]
    TUN {},
}

#[derive(Serialize, Deserialize, PartialEq, Debug)]
#[serde(tag = "type")]
pub enum Redirector {
    #[serde(rename = "socks5")]
    Socks5(socks5::redirector::Config),
    #[serde(rename = "ss")]
    Shadowsocks(ShadowsocksConfig),
}

#[derive(Serialize, Deserialize, PartialEq, Debug)]
pub struct Socks5Config {
    pub server: String,
    pub port: u16,
}

#[derive(Serialize, Deserialize, PartialEq, Debug)]
pub struct ShadowsocksConfig {
    pub server: String,
    pub port: u16,
    pub password: String,
    pub cipher: String,
}

pub fn from_file(f: &File) -> Result<Config, serde_json::error::Error> {
    serde_json::from_reader(f)
}

#[cfg(test)]
mod tests {
    use super::*;
    use std::error::Error;
    #[test]
    fn test_from_file() -> Result<(), Box<dyn Error>> {
        let f = File::open("./tests/data/config_test.json")?;
        let config = from_file(&f)?;
        let mut want_proxies: HashMap<String, Redirector> = HashMap::new();
        want_proxies.insert(
            String::from("DEFAULT"),
            Redirector::Socks5(socks5::redirector::Config {
                server: String::from("127.0.0.1"),
                port: 1080,
            }),
        );
        let want = Config {
            mode: String::from("rule"),
            log: String::from("info"),
            listen: Listen::Socks5 {
                addr: String::from("127.0.0.1:1081"),
            },
            proxies: want_proxies,
            rules: vec![
                String::from("DOMAIN-SUFFIX,google.com,auto"),
                String::from("DOMAIN,twitter.com,auto"),
                String::from("GEOIP,CN,DIRECT"),
                String::from("IP-CIDR,127.0.0.0/8,DIRECT"),
                String::from("SRC-IP-CIDR,192.168.1.201/32,DIRECT"),
                String::from("GEOIP,CN,DIRECT"),
                String::from("DST-PORT,80,DIRECT"),
                String::from("SRC-PORT,7777,DIRECT"),
                String::from("MATCH,auto"),
                String::from("USER-AGENT,MicroMessenger Client*,DIRECT"),
                String::from("DOMAIN-KEYWORD,.tmall.com,DIRECT"),
            ],
        };
        assert_eq!(config, want);
        println!("{:#?}", config);
        Ok(())
    }
}
