# Core Concept

- Listener: Accepting incoming request and send it to `Dispatcher`
- Dispatcher: Matching rules and deciding which `Redirector` to use
- Redirector: Connecting to proxy and forwarding data between streams

This is the sequence diagram in most of cases (Not accurate because actual diagram depending on the protocol of listener and proxy). We are trying to keep the concept of connection/stream not leaking from the place it's created because of the lifetime. So instead of using `Listener` and `Dialer`, then forwarding stream in a seperate module, we use a `Redirector`. So the life time of the proxy stream is constrained by the lifetime of the function call `Redirector.redirect`.

```mermaid
sequenceDiagram
    participant [ExodusMain]
    participant Client
    participant [Listener]
    participant (Task)
    participant [Dispatcher]
    participant [Redirector]
    participant ProxyServer
    participant Target
    [ExodusMain] ->> [Listener]: Create Listener
loop Accepting incoming requests
    Client ->> [Listener]: Connect to Target
    [Listener] ->> [Listener]: Accept
    [Listener] -->> (Task): Spawn
end
    (Task) ->> [Dispatcher]: Dispatcher.redirect(target, src)
    [Dispatcher] ->> [Dispatcher]: Select Redirector by Rules
    [Dispatcher] ->> [Redirector]: Redirector.redirect(target, src)
    [Redirector] -x ProxyServer: Send target
    ProxyServer ->> Target: Connect
loop Abstract connection from Client to Target
    Client ->> [Redirector]: Send data
    [Redirector] ->> ProxyServer: Send data
    ProxyServer ->> Target: Send data
    Target ->> ProxyServer: Send data
    ProxyServer ->> [Redirector]: Send data
    [Redirector] ->> Client: Send data
end
```