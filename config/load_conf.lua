local inspect = require("inspect")
local json = require("json")

local function load_conf()
   local config = {
      port = 1080,
      mode = "Rule",
      log = "info",
      allowLan = false,
      proxies = {},
      proxyGroups = {},
      rules = {},
   }

   local env = {
      dns = function(conf)
         config.dns = conf
      end,
      proxy = function(name, conf)
         return function(conf)
            config.proxies[name] = conf
         end
      end,
      proxyGroup = function(name, conf)
         return function(conf)
            config.proxyGroups[name] = conf
         end
      end,
      rule = function(conf)
         if conf.target then
            table.insert(
               config.rules,
               conf.type .. "," .. conf.target .. "," .. conf.policy
            )
         elseif conf.targets then
            for i, target in ipairs(conf.targets) do
               table.insert(
                  config.rules,
                  conf.type .. "," .. target .. "," .. conf.policy
               )
            end
         end
      end,
      rules = function(ruleList)
         for i, rule in ipairs(ruleList) do
            table.insert(config.rules, rule)
         end
      end,
      exodus = function(conf)
         for k, v in pairs(conf) do
            config[k] = v
         end
      end,
   }

   setmetatable(env, { __index = _G })
   loadfile("config.lua", 't', env)()
   return config
end

-- return load_conf()

local conf = load_conf()
local data = json.encode(conf)
local f = io.open('config.json', 'w')
io.output(f)
io.write(data)
io.close(f)
